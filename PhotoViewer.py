import tkinter as tk
from PIL import ImageTk, Image, ImageFilter
from image_helpers import exif_orientation, list_images_in_dir
import random

class PhotoViewer:
    def __init__(self,fdir,delay=10000):
        self.tk = tk.Tk()
        self.tk.geometry("600x400")
        self.tk.configure(background='grey')
        self.tk.attributes('-fullscreen', True)  # This just maximizes it so we can see the window. It's nothing to do with fullscreen.
        #self.frame = tk.Frame(self.tk)
        #self.frame.pack()
        self.tk.update()
        hframe = self.tk.winfo_height()
        wframe = self.tk.winfo_width()

        self.tk.fdir = fdir
        self.delay = delay
        self.tk.flist = list_images_in_dir(self.tk.fdir)
        
        path = random.choice(self.tk.flist)

        img = self.load_image(path)

        self.panel = tk.Label(self.tk,image=img)
        self.panel.pack(fill='both')
        self.set_random_image()

    def set_random_image(self):
        path = random.choice(self.tk.flist)
        img = self.load_image(path)
        self.panel.configure(image=img)
        self.panel.after(self.delay, self.set_random_image)

    def load_image(self,path):
        self.tk.update()
        hframe = self.tk.winfo_height()
        wframe = self.tk.winfo_width()
        pim = Image.open(path) #load image
        pim = prep_image(pim,(wframe,hframe))
        img = ImageTk.PhotoImage(pim)
        self.photo = img
        return img


def prep_image(im,framedim):
        wframe = framedim[0]
        hframe = framedim[1]
        im = exif_orientation(im) #rotate according to exif orienation
        fg = fit_to_frame(im,(wframe,hframe))#fit it into the frame
        wim,him = fg.size
        bg = fill_frame(im,(wframe,hframe)) #blurry background takes up empty space
        wbg,hbg = bg.size
        bg.paste(fg,(int(wbg/2 - wim/2),int(hbg/2 - him/2)))
        return bg

def fit_to_frame(im,framedim):
        wframe = framedim[0]
        hframe = framedim[1]
        width, height = im.size
        
        if (width/wframe) > (height/hframe): #wide image, match to width of frame
            scale = wframe/width
            im = im.resize((wframe,int(height*scale)))
        else:
            scale = hframe/height
            im = im.resize((int(width*scale),hframe))                  

        return im

def fill_frame(im,framedim,blur=True):
        wframe = framedim[0]
        hframe = framedim[1]
        width, height = im.size
        
        if not (width/wframe) > (height/hframe): #wide image, match to width of frame
            scale = wframe/width
            im = im.resize((wframe,int(height*scale)))
        else:
            scale = hframe/height
            im = im.resize((int(width*scale),hframe))

        if blur:
            im = im.filter(ImageFilter.GaussianBlur(16))
            

        return im
