from PIL import Image
import os
from glob import glob

def get_orientation(img):
    orientation_tag = 274 #key value of orientation
    exif = img._getexif()
    #might need a try catch if that exif field doesn't exist
    if 274 in exif.keys():
        orientation = exif[orientation_tag]
    else:
        orientation = 0
    return orientation

def exif_orientation(img):
    orientation = get_orientation(img)
    method = {
        0: None,
        2: Image.FLIP_LEFT_RIGHT,
        3: Image.ROTATE_180,
        4: Image.FLIP_TOP_BOTTOM,
        5: Image.TRANSPOSE,
        6: Image.ROTATE_270,
        #7: Image.TRANSVERSE,
        8: Image.ROTATE_90,
    }.get(orientation)
    if method is not None:
        transposed_image = img.transpose(method)
        return transposed_image
    else:
        return img.copy()

def list_images_in_dir(fdir,
 #                      ftypes = ['.jpg','.jpeg','.JPG','.JPEG','.png','.PNG'],
                       ftypes = ['.jpg','.jpeg','.JPG','.JPEG'],

                       sort_by_latest=True):
    flist = []
    for ft in ftypes: #iterate through each filetype
        ff = glob(os.path.join(fdir,'*'+ft))
        flist.extend(ff)

    if sort_by_latest:
        flist.sort(key=os.path.getmtime) #sort by modification time (ascending)
        flist.reverse() #make descending (last modified is first)

    return flist

    
