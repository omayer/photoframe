from PhotoViewer import PhotoViewer
import argparse

parser = argparse.ArgumentParser(description='PhotoViewer display')
parser.add_argument('fdir', metavar='f', type=str,
                    help='photo directory',
                    default='/Users/owen/Dropbox/rospics/')
parser.add_argument('--t','-t', metavar='t', type=int,
                    help='time delay between photos',
                    default=10000)

args = parser.parse_args()
w = PhotoViewer(args.fdir,delay=args.t)
w.tk.mainloop()
